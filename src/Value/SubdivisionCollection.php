<?php

declare(strict_types=1);

namespace Drupal\jsonrpc_intl\Value;

use CommerceGuys\Addressing\Subdivision\Subdivision;

/**
 * Value object for a collection of subdivisions.
 */
final class SubdivisionCollection {

  /**
   * Constructor.
   *
   * @param Subdivision[] $subdivisions
   *   Contained subdivisions.
   */
  public function __construct(
    public readonly array $subdivisions,
  ) {}

}
