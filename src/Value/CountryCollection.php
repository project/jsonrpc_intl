<?php

declare(strict_types=1);

namespace Drupal\jsonrpc_intl\Value;

use CommerceGuys\Addressing\Country\Country;

/**
 * Value object for a collection of countries.
 */
final class CountryCollection {

  /**
   * Constructor.
   *
   * @param Country[] $countries
   *   Contained countries.
   */
  public function __construct(
    public readonly array $countries,
  ) {}

}
