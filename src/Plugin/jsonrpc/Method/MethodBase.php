<?php

declare(strict_types=1);

namespace Drupal\jsonrpc_intl\Plugin\jsonrpc\Method;

use Doctrine\Common\Collections\Collection;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Http\RequestStack;
use Drupal\jsonrpc\Exception\JsonRpcException;
use Drupal\jsonrpc\Handler;
use Drupal\jsonrpc\Object\Error;
use Drupal\jsonrpc\Object\Response;
use Drupal\jsonrpc\Plugin\JsonRpcMethodBase;
use Drupal\jsonrpc_intl\CollectionSerializer;
use Drupal\jsonrpc_intl\KeyFormatterForExternal;
use EventSauce\ObjectHydrator\DefaultSerializerRepository;
use EventSauce\ObjectHydrator\DefinitionProvider;
use EventSauce\ObjectHydrator\ObjectMapperUsingReflection;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class MethodBase extends JsonRpcMethodBase {

  protected RequestStack $requestStack;

  public function setRequestStack(RequestStack $requestStack): void {
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setRequestStack($container->get('request_stack'));
    return $instance;
  }

  protected static function getMapper(): ObjectMapperUsingReflection {
    $defaultSerializer = DefaultSerializerRepository::builtIn();
    $defaultSerializer->registerDefaultSerializer(Collection::class, CollectionSerializer::class);
    return new ObjectMapperUsingReflection(
      new DefinitionProvider(
        keyFormatter: new KeyFormatterForExternal(),
        defaultSerializerRepository: $defaultSerializer,
      )
    );
  }

  /**
   * Build a JSON-RPC response object.
   *
   * @param object $responseData
   *   Response data object to serialize.
   *
   * @return Response
   *   Response with cacheability metadata.
   */
  protected function buildResponse(object $responseData): Response {
    if ($this->requestStack->getCurrentRequest()->getMethod() !== 'GET') {
      throw JsonRpcException::fromError(Error::invalidRequest('Must be GET request for this method.'));
    }
    return (new Response(
      Handler::supportedVersion(),
      $this->currentRequest()->id(),
      $this::getMapper()->serializeObject($responseData)
    ))->mergeCacheMaxAge(Cache::PERMANENT);
  }
}
