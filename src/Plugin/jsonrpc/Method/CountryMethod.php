<?php

declare(strict_types=1);

namespace Drupal\jsonrpc_intl\Plugin\jsonrpc\Method;

use CommerceGuys\Addressing\Country\CountryRepositoryInterface;
use Drupal\jsonrpc\Object\ParameterBag;
use Drupal\jsonrpc_intl\Value\CountryCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Retrieve country data by country code.
 *
 * @JsonRpcMethod(
 *   id = "intl.country",
 *   usage = @Translation("Retrieve country definitions."),
 *   params = {
 *     "country_code" = @JsonRpcParameterDefinition(
 *       schema = {"type": "string"},
 *       required = false,
 *     ),
 *   },
 * )
 */
final class CountryMethod extends MethodBase {

  /**
   * Country repository.
   */
  protected CountryRepositoryInterface $countryRepository;

  /**
   * Setter for the country repository.
   *
   * @param CountryRepositoryInterface $countryRepository
   *   Country repository.
   */
  public function setCountryRepository(CountryRepositoryInterface $countryRepository): void {
    $this->countryRepository = $countryRepository;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setCountryRepository($container->get('jsonrpc_intl.country_repository'));
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function execute(ParameterBag $params) {
    return $this->buildResponse(
      $params->has('country_code')
        ? $this->countryRepository->get($params->get('country_code'))
        : new CountryCollection($this->countryRepository->getAll())
    );
  }

  /**
   * {@inheritDoc}
   */
  public static function outputSchema() {
    // @todo Make this more accurate.
    return ['type' => 'object'];
  }

}
