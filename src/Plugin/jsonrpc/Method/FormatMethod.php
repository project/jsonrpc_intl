<?php

declare(strict_types=1);

namespace Drupal\jsonrpc_intl\Plugin\jsonrpc\Method;

use CommerceGuys\Addressing\AddressFormat\AddressFormatRepositoryInterface;
use Drupal\jsonrpc\Object\ParameterBag;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Retrieve address format data by country code.
 *
 * @JsonRpcMethod(
 *   id = "intl.format",
 *   usage = @Translation("Link Instagram with OAuth2 auth code."),
 *   params = {
 *     "country_code" = @JsonRpcParameterDefinition(
 *       schema = {"type": "string"},
 *       required = true,
 *     ),
 *   },
 * )
 */
final class FormatMethod extends MethodBase {

  /**
   * Address format repository.
   */
  protected AddressFormatRepositoryInterface $addressFormatRepository;

  /**
   * Set the address format repository.
   *
   * @param AddressFormatRepositoryInterface $addressFormatRepository
   *   Address format repository.
   */
  public function setAddressFormatRepository(AddressFormatRepositoryInterface $addressFormatRepository): void {
    $this->addressFormatRepository = $addressFormatRepository;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setAddressFormatRepository($container->get('jsonrpc_intl.address_format_repository'));
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function execute(ParameterBag $params) {
    return $this->buildResponse(
      $this->addressFormatRepository->get($params->get('country_code'))
    );
  }

  /**
   * {@inheritDoc}
   */
  public static function outputSchema() {
    // @todo Make this more accurate.
    return ['type' => 'object'];
  }

}
