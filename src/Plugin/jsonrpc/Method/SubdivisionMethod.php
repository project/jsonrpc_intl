<?php

declare(strict_types=1);

namespace Drupal\jsonrpc_intl\Plugin\jsonrpc\Method;

use CommerceGuys\Addressing\Subdivision\SubdivisionRepositoryInterface;
use Drupal\jsonrpc\Object\ParameterBag;
use Drupal\jsonrpc_intl\Value\SubdivisionCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Retrieve address format data by country code.
 *
 * @JsonRpcMethod(
 *   id = "intl.subdivision",
 *   usage = @Translation("Link Instagram with OAuth2 auth code."),
 *   params = {
 *     "parents" = @JsonRpcParameterDefinition(
 *       schema = {"type": "array", "items": {"type": "string"}},
 *       required = false,
 *     ),
 *   },
 * )
 */
final class SubdivisionMethod extends MethodBase {

  /**
   * Subdivision repository.
   */
  protected SubdivisionRepositoryInterface $subdivisionRepository;

  /**
   * Set the subdivision repository.
   *
   * @param SubdivisionRepositoryInterface $subdivisionRepository
   *   Subdivision repository.
   */
  public function setSubdivisionRepository(SubdivisionRepositoryInterface $subdivisionRepository): void {
    $this->subdivisionRepository = $subdivisionRepository;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setSubdivisionRepository($container->get('jsonrpc_intl.subdivision_repository'));
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function execute(ParameterBag $params) {
    return $this->buildResponse(
      new SubdivisionCollection($this->subdivisionRepository->getAll($params->get('parents')))
    );
  }

  /**
   * {@inheritDoc}
   */
  public static function outputSchema() {
    // @todo Make this more accurate.
    return ['type' => 'object'];
  }

}
