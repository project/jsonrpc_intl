<?php

declare(strict_types=1);

namespace Drupal\jsonrpc_intl;

use EventSauce\ObjectHydrator\KeyFormatter;
use EventSauce\ObjectHydrator\KeyFormatterForSnakeCasing;

final class KeyFormatterForExternal implements KeyFormatter {

  /**
   * {@inheritDoc}
   */
  public function propertyNameToKey(string $propertyName): string {
    $snakeCased = (new KeyFormatterForSnakeCasing())
      ->propertyNameToKey($propertyName);
    return str_starts_with($snakeCased, 'get_')
      ? substr($snakeCased, 4)
      : $snakeCased;
  }

  /**
   * {@inheritDoc}
   */
  public function keyToPropertyName(string $key): string {
    throw new \RuntimeException('Not implemented.');
  }

}
