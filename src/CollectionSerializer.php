<?php

declare(strict_types=1);

namespace Drupal\jsonrpc_intl;

use CommerceGuys\Addressing\Subdivision\Subdivision;
use Doctrine\Common\Collections\Collection;
use EventSauce\ObjectHydrator\ObjectMapper;
use EventSauce\ObjectHydrator\PropertySerializer;

/**
 * Serializer for Doctrine collections used in Subdivision value objects.
 *
 * Listing only the code cuts down on potential recursion and subdivision data
 * should be accessible with the proper parents passed to the method.
 */
final class CollectionSerializer implements PropertySerializer {

  /**
   * {@inheritDoc}
   */
  public function serialize(mixed $value, ObjectMapper $hydrator): mixed {
    assert($value instanceof Collection);
    return array_map(fn (Subdivision $s) => ['code' => $s->getCode()], $value->getValues());
  }

}
